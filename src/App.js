import { useEffect, useRef, useState } from 'react';
import './App.scss'; 
import Button from "./components/Button"; 
import Modal from "./components/Modal";
import ProductCard from './components/productCard/ProductCard';
import Header from './components/header/Header';

function App() {
    const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
    const [isAddModalOpen, setIsAddModalOpen] = useState(false);
    const [products, setProducts] = useState([]);
    const selectedProductsId = useRef([]);
    const addedToCartProductsId = useRef([]);
    // Створюємо стан для числа вибраних користувачем товарів, яке потім передаватимемо
    // в компонент Header у вигляді пропсів. Аналогічно діємо з доданими в кошик товарами
    const [selectedProductsCount, setSelectedProductsCount] = useState(0);
    const [addedToCartProductsCount, setAddedToCartProductsCount] = useState(0);

    useEffect(() => {
        const selectedProducts = JSON.parse(localStorage.getItem('selectedProducts')) || [];
        const addedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts')) || [];
        setSelectedProductsCount(selectedProducts.length);
        setAddedToCartProductsCount(addedToCartProducts.length);
    }, []);

    function handleAddToCart(){
        closeModal();
        console.log(addedToCartProductsId);
        let alreadyAddedToCartProducts = [];

        if (localStorage.getItem('addedToCartProducts')) {
            alreadyAddedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts'));
            if(alreadyAddedToCartProducts.includes(addedToCartProductsId.current)) {
                alert('You`ve already added this product to a cart!');
                return
            }
        }
        localStorage.setItem('addedToCartProducts', JSON.stringify([...alreadyAddedToCartProducts, addedToCartProductsId.current]));
        // Оновлюємо кількість доданих у кошик товарів
        updateAddedToCartProductsCount();
    }

    // Функція для оновлення кількості обраних товарів, що визиватиметься компонентом
    // ProductCard після кожного кліку по starIcon
    function updateSelectedProductsCount() {
        const selectedProducts = JSON.parse(localStorage.getItem('selectedProducts'));
        setSelectedProductsCount(selectedProducts.length);
    }

    // Функція для оновлення кількості доданих у кошик товарів
    function updateAddedToCartProductsCount() {
        const addedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts'));
        setAddedToCartProductsCount(addedToCartProducts.length);
    }

    useEffect(()=>{
        let selectedProductsFromStorage = [];
        // Отримуємо айді обраних товарів з локал стореджу
        // Будемо передавати це значення в пропси до компоненту ProductCard
        // аби додані в обране товари зберігалися після перезавантаження сторінки
        if(localStorage.getItem('selectedProducts')){
            selectedProductsFromStorage = JSON.parse(localStorage.getItem('selectedProducts'));
        }
        console.log('Функция selProds сработала');
        selectedProductsId.current = selectedProductsFromStorage;
        console.log(selectedProductsFromStorage);
        console.log(selectedProductsId.current);
    }, [])

    // Використовуємо хук useRef щоб зберігати значення скролу сторінки
    // useState ми не можемо використати, бо функція setState має асихронну природу,
    // тому ми отримуватимемо старі (минулі) значення scrollY, тоді як useRef
    // дозволяє зберігати тільки актуальне значення
    const scrollY = useRef(0); 

    function openDeleteModal(){
        setIsDeleteModalOpen(true);
    }

    function openAddModal(event, id){
        scrollY.current = window.scrollY;
        setIsAddModalOpen(true);
        document.body.style.position = 'fixed';
        document.body.style.top = `-${scrollY.current}px`;
        document.body.style.paddingLeft = '32px';
        
        //Зберігаємо айді доданого в кошик товару
        addedToCartProductsId.current = id;
        console.log(addedToCartProductsId.current);
    }

    function closeModal(){
        setIsDeleteModalOpen(false);
        setIsAddModalOpen(false);
        document.body.style.position = '';
        document.body.style.top = ``;
        window.scrollTo(0, scrollY.current);
        document.body.style.paddingLeft = '';
    }

    useEffect(()=>{
        // Fetch запит на JSON файл з папки public
        fetch('products.json')
        .then(res => res.json())
        .then(data => setProducts(data.products))
        .catch(error => console.log('An error occured while fetching all products: ', error))
    }, [])

    return (
        <div className='App' >
            <Header selectedProductsCount={selectedProductsCount} 
                    addedToCartProductsCount={addedToCartProductsCount}
            />
            <div class="cardsWrapper">
                {products.map((product) => <ProductCard
                                                key={product.setNumber}
                                                updateSelectedProductsCount={updateSelectedProductsCount}
                                                selectedProductsId={selectedProductsId.current}
                                                id={product.id}
                                                name={product.name}
                                                price={product.price}
                                                description={product.description}
                                                color={product.color}
                                                imgUrl={product.imgUrl}
                                                openAddModal={openAddModal}
                                            />)}
            </div>
            {isDeleteModalOpen && 
                <Modal
                backgroundColor='linear-gradient(to bottom, #D44638 24%, #E74C3D 24% 100%)'
                header='Do you want to delete this file?'
                closeButton='true'
                // closeButton='false'
                text='Once you delete this file it won`t be possible to undo this action.
                Are you sure you want to delete it?'
                actions={<><button className='buttonForDeleteModal'>Ok</button>
                <button className='buttonForDeleteModal'>Cancel</button></>}
                closeModal={closeModal}
            />
            }

            {isAddModalOpen && 
                <Modal
                backgroundColor='linear-gradient(to bottom, #348a0c 33%, #269618 24% 100%)'
                header='Do you want to add this product to your cart?'
                closeButton='true'
                // closeButton='false'
                text='If you choose No you, you can still add it later'
                actions={<><button className='buttonForAddModal' onClick={handleAddToCart}>Add</button>
                <button className='buttonForAddModal' onClick={closeModal}>No</button></>}
                closeModal={closeModal}
            />
            }
            
            <div className="modalButtonsWrapper" style={{display: 'none'}}>
                <Button
                    backgroundColor='#E74C3D'
                    text='Open first modal'
                    onClick={openDeleteModal}
                />
                <Button
                    backgroundColor='#269618'
                    text='Open second modal'
                    onClick={openAddModal}
                />
            </div>
        </div>
    );
}

export default App;
